<?php

/**
 * @author Siomkin Alexandr <mail@mg7.by>
 * @link http://www.jext.biz/
 * @copyright Copyright &copy; 2008-2012
 * @license GNU General Public License, version 2:
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

class Billing_RbkmoneyController extends Zend_Controller_Action
{
    protected $config;

    protected $basic_account;

    public function init()
    {
        if ($this->view->identity == false) {
             $this->_redirect('/');
        }


        $this->config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/billing.ini', 'app');

        $this->basic_account = $this->view->identity->basic_account;

        $rbk = array();
        $rbk['secretKey'] = '**************'; // Берем из настроек магазина на сайте РБК
        $rbk['eshopId'] = '*********'; //Идентификатор вашего магазина. Берем из настроек магазина на сайте РБК
        $rbk['orderId']= time(); //Номер покупки
        $rbk['recipientCurrency'] = "RUR";
        $rbk['serviceName'] = 'Пополнение лицевого счета ' . $this->basic_account;
        $rbk['successUrl'] = "https://***********/rbkmoney?result=ok"; //Замените на ваш путь
        $rbk['failUrl'] = "https://***********/rbkmoney?result=error"; //Замените на ваш путь
        $this->view->rbk = $rbk;
    }

    /**
     * Экшен, обеспечивающий вывод информации о пользователе
     */
    public function indexAction()
    {
        if (!is_null($this->_getParam('result'))) {
            if ($this->_getParam('result') === 'ok') {
                $this->view->message = array('success'=>'Платеж принят на обработку, зачисление средств произойдет через несколько минут.');
            }
            elseif ($this->_getParam('result') === 'error') {
                $this->view->message = array('error'=>'Ошибка платежа');
            }
        } else {


        }
        $this->view->title = "Оплата RBK Money";
        $this->view->headTitle($this->view->title, 'PREPEND');


    }
}